import { Navbar, NavbarBrand, Form } from "react-bootstrap";
import { Button, NavbarText } from "reactstrap";
import { useState } from "react";
import { updateProfile } from "firebase/auth";
import { auth } from "../../firebase";
import axios from "axios";

const Profile = () => {
  const [city, setCity] = useState("");
  const [displayName, setDisplayName] = useState("");
  const [address, setAddress] = useState("");
  const [phone, setPhone] = useState("");

  const handleUpdate = (e) => {
    e.preventDefault();

    try {
      updateProfile(auth.currentUser, {displayName: displayName}).then(() => {
        const user = auth.currentUser;
        const userId = user.uid;

        axios.put("http://localhost:8000/api/update", {
          uid: userId,
          name: displayName,
          city: city,
          address: address,
          phone: phone,
        });
        console.log("uid", userId);
      });
    } catch (error) {
      // Handle Errors here.
      console.log(error.code);
      console.log(error.message);
    }
  };


  return (
    <div>
      <div className="container">
        <Navbar color="" light container header class>
          <NavbarBrand href="/">
            <img
              src="img/icon.png"
              style={{ width: "100px" }}
              className="icon h-84px"
              alt=""
            />
          </NavbarBrand>
          <NavbarText>
            <h3 className="text-center">Lengkapi info Akun</h3>
          </NavbarText>
        </Navbar>
      </div>
      <div className="container-fluid p-0">
        <div className="row justify-content-md-center">
          <div className="col-md-4 col-lg-4 col-xs-12">
            <Form className="sign" onSubmit={handleUpdate}>
              <fieldset>
                <Form.Group className="mb-3">
                  <Form.Label className="form-label">Nama</Form.Label>
                  <Form.Control
                    className="inputBasic"
                    id="nama"
                    placeholder="Nama"
                    onChange={(e) => setDisplayName(e.target.value)}
                  />
                </Form.Group>
                <Form.Group className="mb-3">
                  <Form.Label className="form-label">Kota</Form.Label>
                  <Form.Select
                    id="city"
                    className="inputBasic"
                    onChange={(e) => setCity(e.target.value)}
                  >
                    <option selected disabled>
                      Pilih Kota
                    </option>
                    <option value={"Jakarta"}>Jakarta</option>
                    <option value={"Surabaya"}>Surabaya</option>
                  </Form.Select>
                </Form.Group>
                <Form.Group className="mb-3">
                  <Form.Label className="form-label">Alamat</Form.Label>
                  <Form.Control
                    className="inputBasic text-input"
                    id="kota"
                    placeholder="Contoh: Jalan Ikan Hiu 33"
                    onChange={(e) => setAddress(e.target.value)}
                  />
                </Form.Group>
                <Form.Group className="mb-3">
                  <Form.Label className="form-label">No. Handphone</Form.Label>
                  <Form.Control
                    className="inputBasic text-input"
                    id="kota"
                    placeholder="Contoh: +628123456789"
                    onChange={(e) => setPhone(e.target.value)}
                  />
                </Form.Group>
                <div className="d-grid gap-2 mb-3">
                  <Button
                    type="submit"
                    className="btn-sign text-center"
                  >
                    Simpan
                  </Button>
                </div>
              </fieldset>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Profile;
