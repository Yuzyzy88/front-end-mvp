import React from "react";
import { useState } from "react";
import { Button, Form } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { auth } from "../../firebase";
import { useNavigate } from "react-router-dom";
import {
  createUserWithEmailAndPassword,
  signInWithPopup,
  GoogleAuthProvider,
} from "firebase/auth";
import axios from "axios";

const provider = new GoogleAuthProvider();

const Register = () => {
  const [error, setError] = useState(false);
  const [displayName, setDisplayName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const navigate = useNavigate();

  const handleLogin = (e) => {
    e.preventDefault();

    try {
      createUserWithEmailAndPassword(auth, email, password).then(
        (userCredential) => {
          // Signed in
          const user = userCredential.user;
          const userId = user.uid;
          axios.post("http://localhost:8000/api/register", {
            uid: userId,
            name: displayName,
          });
          console.log("uid", userId);
          navigate("/profile");
        }
      );
    } catch (error) {
      setError(true);
    }
  };

  const handleLoginGoogle = (e) => {
    e.preventDefault();

    try {
      signInWithPopup(auth, provider).then((result) => {
        // This gives you a Google Access Token. You can use it to access the Google API.
        const credential = GoogleAuthProvider.credentialFromResult(result);
        const token = credential.accessToken;
        const userresult = result.user;
        const userId = result.user.uid;
        const userName = result.user.displayName;
        axios.post("http://localhost:8000/api/register", {
          uid: userId,
          name: userName,
        });
      console.log(userresult);
      navigate("/profile");
      });
    } catch (error) {
      // Handle Errors here.
      console.log(error.code);
      console.log(error.message);
    }
  };

  return (
    <div className="container-fluid">
      <div className="row align-items-center">
        <div className="col-lg-6 col-md-12 d-flex p-0">
          <img
            className="img-fluid icon"
            src="/img/bg.png"
            alt=""
            width="100%"
            style={{ height: "967px" }}
          />
        </div>
        <div className="col-lg-6 col-md-12 col-xs-12 pt-5 pb-5">
          <div className="row justify-content-center">
            <div className="col-md-8 col-lg-6 col-xs-12">
              <Form className="sign" onSubmit={handleLogin}>
                <h4 className="pb-2 fw-bold">Daftar</h4>
                <fieldset>
                  <Form.Group className="mb-3">
                    <Form.Label className="form-label">Name</Form.Label>
                    <Form.Control
                      className="inputBasic"
                      id="displayName"
                      placeholder="Contoh: John dee"
                      onChange={(e) => setDisplayName(e.target.value)}
                    />
                  </Form.Group>
                  <Form.Group className="mb-3">
                    <Form.Label className="form-label">Email</Form.Label>
                    <Form.Control
                      className="inputBasic"
                      id="email"
                      placeholder="Contoh: johndee@gmail.com"
                      onChange={(e) => setEmail(e.target.value)}
                    />
                  </Form.Group>
                  <Form.Group className="mb-3">
                    <Form.Label htmlFor="textInput">Password</Form.Label>
                    <Form.Control
                      className="inputBasic"
                      id="password"
                      placeholder="Masukkan password"
                      onChange={(e) => setPassword(e.target.value)}
                    />
                  </Form.Group>
                  <div className="d-grid gap-2 mb-4">
                    <Button type="submit" className="btn-sign text-center">
                      Daftar
                    </Button>
                    {error && <span>Fill out email or password!</span>}
                  </div>
                  <div className="text-center mb-3">
                    Sudah punya akun?{" "}
                    <a
                      href="/login"
                      className="link-primary text text-decoration-none fw-bold"
                    >
                      Masuk di sini
                    </a>
                  </div>
                  <div className="d-grid gap-2">
                    <Button
                      type="submit"
                      className="btn-sign-google text-center"
                      onClick={handleLoginGoogle}
                    >
                      <img
                        src="/img/google.png"
                        alt=""
                        width={24}
                        className="me-2"
                      />
                      Sign In With Google
                    </Button>
                  </div>
                </fieldset>
              </Form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Register;
