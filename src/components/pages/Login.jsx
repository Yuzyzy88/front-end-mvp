import React, { useContext } from "react";
import { useState } from "react";
import { Button, Form } from "react-bootstrap";
import { auth } from "../../firebase";
import "bootstrap/dist/css/bootstrap.min.css";
import { useNavigate } from "react-router-dom";
import { signInWithEmailAndPassword } from "firebase/auth";

const Login = () => {
  const [error, setError] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const navigate = useNavigate();

  const handleLogin = (e) => {
    e.preventDefault();
    signInWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        // Signed in
        const user = userCredential.user;
        console.log(user);
        navigate("/profile");
      })
      .catch((error) => {
        setError(true);
      })
  };

  return (
    <div className="container-fluid">
      <div className="row align-items-center">
        <div className="col-lg-6 col-md-12 d-flex p-0">
          <img
            className="img-fluid icon"
            src="/img/bg.png"
            alt=""
            width="100%"
            style={{ height: "967px" }}
          />
        </div>
        <div className="col-lg-6 col-md-12 col-xs-12 pt-5 pb-5">
          <div className="row justify-content-center">
            <div className="col-md-8 col-lg-6 col-xs-12">
              <Form className="sign" onSubmit={handleLogin}>
                <h4 className="pb-2 fw-bold">Masuk</h4>
                <fieldset>
                  <Form.Group className="mb-3">
                    <Form.Label className="form-label">Email</Form.Label>
                    <Form.Control
                      className="inputBasic"
                      id="email"
                      placeholder="Contoh: johndee@gmail.com"
                      onChange={(e) => setEmail(e.target.value)}
                    />
                  </Form.Group>
                  <Form.Group className="mb-1">
                    <Form.Label htmlFor="textInput">Password</Form.Label>
                    <Form.Control
                      className="inputBasic"
                      id="password"
                      placeholder="Masukkan password"
                      onChange={(e) => setPassword(e.target.value)}
                    />
                  </Form.Group>
                  <Form.Group className="mb-3">
                    <div className="row">
                      <div className="col auto small">
                        <Form.Check
                          type="checkbox"
                          id="rememberMe"
                          label="Ingat saya"
                        />
                      </div>
                      <div className="col auto text-end small">
                        <a
                          href="/lupa-password"
                          className="link-primary text text-decoration-none"
                        >
                          Lupa Password
                        </a>
                      </div>
                    </div>
                  </Form.Group>
                  <div className="d-grid gap-2 mb-3">
                    <Button type="submit" className="btn-sign text-center">
                      Masuk
                    </Button>
                    {error && <span>Wrong email or password!</span>}
                  </div>
                  <div className="text-center mb-4">
                    Belum punya akun?{" "}
                    <a
                      href="/register"
                      className="link-primary text text-decoration-none fw-bold "
                    >
                      Daftar di sini
                    </a>
                  </div>
                  <div className="d-grid gap-2">
                    <Button
                      type="submit"
                      className="btn-sign-google text-center"
                    >
                      <img
                        src="/img/google.png"
                        alt=""
                        width={24}
                        className="me-2"
                      />
                      Sign Up With Google
                    </Button>
                  </div>
                </fieldset>
              </Form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
