import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css"
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import './style/App.css';
import { Routes, Route } from 'react-router-dom';
import Main from './components/pages/Main';
import Banner from './components/Banner';
import Cater from './components/Cater';
import Login from "./components/pages/Login";
import Register from "./components/pages/Register";
import LupaPassword from './components/pages/LupaPassword';
import Profile from './components/pages/Profile';

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" 
        element={
        <Main 
          compBanner={<Banner />}
          compCater={<Cater />}
          />
        }/>
        <Route
          path="/login"
          element={<Login />}
        />
        <Route
          path="/register"
          element={<Register />}
        />
        <Route
          path="/lupa-password"
          element={<LupaPassword/>}
        />
        <Route
          path="/profile"
          element={<Profile/>}
        />
      </Routes>
    </div>
  );
}

export default App;
