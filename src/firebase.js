import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";


const firebaseConfig = {
  apiKey: process.env.REACT_APP_FIREBASE_KEY,
  authDomain: "kelompok-3-d7406.firebaseapp.com",
  projectId: "kelompok-3-d7406",
  storageBucket: "kelompok-3-d7406.appspot.com",
  messagingSenderId: "21487527614",
  appId: "1:21487527614:web:35983e766ae04ccd9de0ef"
};

const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);
export const auth = getAuth();
export const storage = getStorage(app);